#include <iostream>
#include "rsync.h"

int main(int argc, char const *argv[]) {
	if (argc == 3) {
		std::string source(argv[1]), dest(argv[2]);
		rsync(source, dest, 0, "host", "port"); // 0 - default // 1 - sender // 2 - receiver 
	} else if (strcmp(argv[1], "-s") == 0) {
		std::string source(argv[3]), dest(argv[4]), port(argv[2]);
		rsync(source, dest, 1, "host", port);			
	} else if (strcmp(argv[1], "-r") == 0) {
		std::string host(argv[2]), port(argv[3]);
		rsync("", "", 2, host, port);
	} else {
		std::cout << "invalid arguments" << std::endl;

	}
	return 0;
}	