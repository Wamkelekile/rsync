#include "rsync.h"

#include <sstream>
#include <thread>
#include <dirent.h>
#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include <dirent.h>
#include <cstring>
#include <algorithm>
#include <unistd.h>
#include <netinet/in.h>

using namespace std;

int Sender::run() {
    this->SendGetList(this->dest_);
    bool state = true;
    while (state) { 
        this->RecvMsg();
        switch (this->last_received_.msg_id) {
            case MsgId::FILELIST: {
                FileList dest_files = this->ReceiveFileList();
                FileList source_files = GetFullDirFileList(this->source_, "");
                std::pair < std::vector<std::string>, std::vector<std::string> >diff = difference(source_files.files, dest_files.files);
                for (auto it = diff.first.begin(); it != diff.first.end(); ++it) {
                    cout << "cp" << ' ' << (*it) << endl;
                }
                for (auto it = diff.second.begin(); it != diff.second.end(); ++it) {
                    cout << "rm" << ' ' << (*it) << endl;
                }

                // do remove
                for (auto it = diff.second.begin(); it != diff.second.end(); ++it) {
                    cout << "Removing file " << *it << "..." << endl;
                    this->SendRemoveFile(*it);
                    this->RecvMsg();
                    if (last_received_.msg_id == MsgId::REMOVE_OK) cout << "OK" << endl;
                }
                for (auto it = diff.first.begin(); it != diff.first.end(); ++it) {

                    cout << "Sending file " << *it << "..." << endl;
                    uint32_t file_size;
                    this->SendFileStat(source_, *it, file_size);
                    this->RecvMsg();

                    if (last_received_.msg_id == MsgId::FILE_OK) { 
                        // cout << "stts_OK" << endl;
                    }
                    int file_fd = open((source_ + *it).c_str(), O_RDONLY);
                    uint32_t parts_count = file_size / 1024;
                    for (size_t i = 0; i <= parts_count; ++i) {
                        size_t count = (i == parts_count) ? (file_size - i * 1024) : 1024;
                        this->SendFilePart(file_fd, count);
                        this->RecvMsg();
                        if (this->last_received_.msg_id == MsgId::FILE_PART_OK) {
                            // cout << "file part " << i << " sent!" << endl;
                        }
                    }
                }
                this->SendOk(MsgId::OK);

                state = false;
                break;
            }

            case MsgId::MESSAGE: {
                int num = atoi(last_received_.body.c_str());
                cout << "NUM1:"<< endl;
                this->SendMessage(1);
                break;
            }
        }
    }
    cout << "Sender DONE" << endl;
    return 0;
}

int Receiver::run() {
    bool state = true;
    while (state) {
        this->RecvMsg();
        switch (this->last_received_.msg_id) {
            case MsgId::GETLIST: {
                this->dest_ = this->last_received_.body;
                FileList files = GetFullDirFileList(this->last_received_.body, "");
                this->SendFileList(files);
                break;
            }
            case MsgId::REMOVE: {
                // REmove file
                this->RemoveFile(dest_);
                this->SendOk(MsgId::REMOVE_OK);
                break;

            }
            case MsgId::MESSAGE: {
                int num = atoi(last_received_.body.c_str());
                cout << "NUM2:"<< endl;
                this->SendMessage(1);
                break;
            }
            case MsgId::FILE: {
                FileStat info = this->ReceiveFileStat();
                string new_file_path = dest_ + info.path;
                int new_file_fd = creat(new_file_path.c_str(), info.rools);
                if (new_file_fd == -1) throw runtime_error("Can't create file");
                this->SendOk(MsgId::FILE_OK);
                uint32_t parts_count = info.size / 1024;
                for (uint32_t i = 0; i <= parts_count; ++i) {
                    size_t count = (i == parts_count) ? (info.size - i * 1024) : 1024;
                    this->RecvMsg();
                    this->ReceiveFilePart(new_file_fd, count);
                    this->SendOk(MsgId::FILE_PART_OK);
                }
                cout << "OK" << endl;
                break;
            }
            case MsgId::OK: {
                state = false;
                break;
            }
            default: {}
        }
    }
    cout << "Receiver DONE" << endl;
    return 0;
}

void readall(int fd, char* ptr, size_t count) {
    uint32_t done = 0;
    while (int well_red = read(fd, ptr + done, count)) {
        count -= well_red;
        done += well_red;
    }
}

void writeall(int fd, char* ptr, size_t count) {
    uint32_t done = 0;
    while (int well_wrote = write(fd, ptr + done, count)) {
        count -= well_wrote;
        done += well_wrote;
    }
}

void Protocol::SendFilePart(int &fd, size_t count) {
    Frame f;
    f.body.resize(count);
    readall(fd, const_cast<char*>(f.body.c_str()), 1024); //??
    f.msg_id = MsgId::FILE_PART;
    conn_->WriteFrame(&f);
}

void Protocol::ReceiveFilePart(int &fd, size_t count) {
    writeall(fd, const_cast<char*>(last_received_.body.c_str()), count);
}

void Protocol::SendFileStat(string path, string name, uint32_t &size) {
    FileStat info;
    struct stat file_stat;
    string full_path = path + name;
    lstat(full_path.c_str(), &file_stat);
    
    info.path = name;
    info.size = file_stat.st_size;
    size = file_stat.st_size;
    info.rools = 00;
    
    if (file_stat.st_mode & S_IRUSR) info.rools |= S_IRUSR;
    if (file_stat.st_mode & S_IWUSR) info.rools |= S_IWUSR;
    if (file_stat.st_mode & S_IXUSR) info.rools |= S_IXUSR;
    if (file_stat.st_mode & S_IRGRP) info.rools |= S_IRGRP;
    if (file_stat.st_mode & S_IWGRP) info.rools |= S_IWGRP;
    if (file_stat.st_mode & S_IXGRP) info.rools |= S_IXGRP;
    if (file_stat.st_mode & S_IROTH) info.rools |= S_IROTH;
    if (file_stat.st_mode & S_IWOTH) info.rools |= S_IWOTH;
    if (file_stat.st_mode & S_IXOTH) info.rools |= S_IXOTH;
    Frame f;
    stringstream ss;
    boost::archive::text_oarchive archive(ss);
    archive << info;
    f.body = ss.str();
    f.msg_id = MsgId::FILE;
    conn_->WriteFrame(&f);
}

FileStat Protocol::ReceiveFileStat() {
    stringstream ss;
    ss << last_received_.body;

    FileStat info;
    boost::archive::text_iarchive archive(ss);
    archive >> info;
    return info;

}

void Protocol::SendMessage(int num) {
    sleep(1);
    Frame f;
    f.msg_id = MsgId::MESSAGE;
    f.body = "";
    conn_->WriteFrame(&f);
} 



void Protocol::SendOk(MsgId state) {
    Frame f;
    f.msg_id = state;
    f.body = "";
    conn_->WriteFrame(&f);

}

void Protocol::SendRemoveFile(string path) {
    Frame f;    
    f.msg_id = MsgId::REMOVE;
    f.body = path;
    conn_->WriteFrame(&f);
}

void Protocol::RemoveFile(string dest) {
    string full_path = dest + last_received_.body;
    int removed = remove(full_path.c_str());
    if (removed == -1) throw runtime_error("Can`t delete the file");

}

void Protocol::ReceiveOk(MsgId state) {
    this->RecvMsg();
    if (last_received_.msg_id == state) cout << "OK" << endl;
        else throw runtime_error("Can't delete the file");

}

void Protocol::SendGetList(const string dest) {
    Frame f;
    f.msg_id = MsgId::GETLIST;
    f.body = dest;  
    conn_->WriteFrame(&f);
}

FileList GetFullDirFileList(string root, string subdir) {
    string full_dir_path = root + subdir;
    FileList list, calc_files;
    std::vector<string> dirsindir;
    DIR *dir;
    struct dirent *entry;
    dir = opendir(full_dir_path.c_str());
    if (dir == NULL) throw runtime_error("Can't open folder");
    if (dir == NULL) return list;
    while((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) continue;
        struct stat file_stat;
        string s(entry->d_name);
        string full_object_path = full_dir_path + "/" + s;
        lstat(full_object_path.c_str(), &file_stat);
        if (S_ISDIR(file_stat.st_mode) && (access(full_object_path.c_str(), R_OK) >= 0)) {
            dirsindir.push_back(subdir + "/" + s);
        }
        if (S_ISREG(file_stat.st_mode)) {
            list.files.push_back(subdir + "/" +  s);
        }
    }
    for (auto i = dirsindir.begin(); i != dirsindir.end(); ++i) {
        calc_files = GetFullDirFileList(root, *i);
        list.files.insert(list.files.end(), calc_files.files.begin(), calc_files.files.end());
    }
    closedir(dir);

    return list;
}


void Protocol::SendFileList(const FileList& list) {
    Frame frame;
    stringstream ss;
    boost::archive::text_oarchive archive(ss);
    archive << list;
    frame.body = ss.str();
    frame.msg_id = MsgId::FILELIST;
    conn_->WriteFrame(&frame);

}

FileList Protocol::ReceiveFileList() {
    stringstream ss;
    ss << last_received_.body;

    FileList list;
    boost::archive::text_iarchive archive(ss);
    archive >> list;
    return list;
}

std::pair < std::vector<string>, std::vector<string> > difference (std::vector<string> source, std::vector<string> dest) { //вектор cp / вектор rm
    sort (source.begin(), source.end());
    sort (dest.begin(), dest.end());
    vector<string> to_copy(source.size());
    vector<string> to_remove(dest.size());
    vector<string>::iterator ptr;
    ptr = set_difference(source.begin(), source.end(), dest.begin(), dest.end(), to_copy.begin()); 
    to_copy.resize(ptr - to_copy.begin()); 
    ptr = set_difference(dest.begin(), dest.end(), source.begin(), source.end(), to_remove.begin()); 
    to_remove.resize(ptr - to_remove.begin());

    pair <vector<string>, vector<string>> result;
    result.first = to_copy; result.second = to_remove;   
    return result; 
}


void SocketConnection::WriteFrame(Frame* frame) {
    uint32_t len = frame->body.length();
    writeall(fd_, (char*) &len, 4);
    uint8_t id = static_cast<uint8_t> (frame->msg_id);
    writeall(fd_, (char*) &id, 1);
    writeall(fd_, (char*) frame->body.c_str(), len);
}

MsgId Protocol::RecvMsg() {
    conn_->ReadFrame(&last_received_);
    return last_received_.msg_id;
}


void SocketConnection::ReadFrame(Frame* frame) {
    uint32_t len;
    readall(fd_, (char*) &len, 4);
    uint8_t msg_id;
    readall(fd_, (char*) &msg_id, 1);
    frame->msg_id = MsgId(msg_id);
    frame->body.resize(len);
    readall(fd_, (char*) frame->body.c_str(), len);
}

int BuildServer(string port) {
    int sock, listener;
    struct sockaddr_in addr;

    listener = socket(AF_INET, SOCK_STREAM, 0);
    if(listener < 0) {
        perror("socket");
        exit(1);
    }
    
    addr.sin_family = AF_INET;
    int portt = atoi(port.c_str());
    addr.sin_port = htons(portt); //7777
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    if(bind(listener, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
        perror("bind");
        exit(2);
    }

    listen(listener, 1);

    sock = accept(listener, NULL, NULL);
    if(sock < 0) {
        perror("accept");
        exit(3);
    }
    return sock;
}

int MakeClient(string host, string port) {
    usleep(100);

    int sock;
    struct sockaddr_in addr;
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if(sock < 0) {
        perror("socket");
        exit(1);
    }
    addr.sin_family = AF_INET;
    int portt = atoi(port.c_str());
    addr.sin_port = htons(portt); // 7777
    
    // string str("127.0.0.1");

    uint32_t b0, b1, b2, b3;
    sscanf(host.c_str(), "%i.%i.%i.%i", &b3, &b2, &b1, &b0); // %i принимает и 8-, и 10-, и 16-ричные числа
    uint32_t ip = (b3 << 24) | (b2 << 16) | (b1 << 8) | (b0);
    addr.sin_addr.s_addr = htonl(ip); // 127.0.0.1 0x7f000001
    if(connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
        perror("connect");
        exit(2);
    }
    return sock;
}


void rsync(string source, string dest, int mod, string host, string port) {
    if (mod == 0) {
        std::thread t1([&] { // sender (source)
            int sock = BuildServer("7777");
            SocketConnection s(sock);
            Sender sender(&s, source, dest);
            sender.run();
        });
        std::thread t2([&] { // receiver (dest) 
            int sock = MakeClient("127.0.0.1", "7777");
            SocketConnection r(sock);
            Receiver receiver(&r);
            receiver.run();
        });
        t1.join();
        t2.join();
    } else if (mod == 1) {
        std::thread t1([&] { // sender (source)
            int sock = BuildServer(port);
            SocketConnection s(sock);
            Sender sender(&s, source, dest);
            sender.run();
        });
        t1.join();
    } else if (mod == 2) {
        std::thread t2([&] { // receiver (dest) 
            int sock = MakeClient(host, port);
            SocketConnection r(sock);
            Receiver receiver(&r);
            receiver.run();
        });
        t2.join();
    }
}