#pragma once

#include <vector>
#include <string>
#include <utility>
#include <fcntl.h>

#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

enum class MsgId {
    GETLIST = 1,
    FILELIST = 2,
    REMOVE = 3,
    REMOVE_OK = 4,
    FILE = 5, //
    FILE_PART = 6,
    OK = 7,
    MESSAGE = 8,
    FILE_OK = 9,
    FILE_PART_OK = 10
};

struct Frame {
    MsgId msg_id;
    std::string body;
};

class Connection {
public:
    virtual ~Connection() {}
    virtual void WriteFrame(Frame* frame) = 0;
    virtual void ReadFrame(Frame* frame) = 0;
};

class SocketConnection {
public:
    SocketConnection(int fd) : fd_(fd) {}
   // ~SocketConnection();

    virtual void WriteFrame(Frame* frame);
    virtual void ReadFrame(Frame* frame);

private:
    int fd_;
};

struct FileList {
    std::vector<std::string> files;

    template<class Archive>
    void serialize(Archive& ar, const unsigned int version) {
        ar & files;
    }
};

struct FileStat {
    std::string path;
    uint32_t rools;
    uint32_t size;
    template<class Archive>
    void serialize(Archive& ar, const unsigned int version) {
        ar & path;
        ar & rools;
        ar & size;
    }


    
};

class Protocol {
public:
    void SendGetList(const std::string dest); // записываем некоторый фрейм с мсгайди 1
    FileList ReceiveFileList();
    void SendRemoveFile(std::string path);
    void SendFileStat(std::string path, std::string name, uint32_t &size); // Функция также записывает размер файла по указателю
    void SendOk(MsgId state);
    void SendFilePart(int &fd, size_t count);

    void SendFileList(const FileList& list);
    void RemoveFile(std::string path);
    FileStat ReceiveFileStat();
    void ReceiveOk(MsgId state);
    void ReceiveFilePart(int &fd, size_t count);

    MsgId RecvMsg(); // reads next frame
    void SendMessage(int num);
    
    Protocol(SocketConnection* conn) : conn_(conn) {}
    ~Protocol() {}
protected:
    SocketConnection* conn_;
    Frame last_received_;
};


class Sender : public Protocol  {
public:
    Sender(SocketConnection* conn, std::string source, std::string dest) : Protocol(conn) {
        // Build server
        source_ = source;
        dest_ = dest;
    }
    int run();
    // void start_server();
    // ~Sender();
private:
    std::string source_;
    std::string dest_;
    
};

class Receiver : public Protocol {
public:
    Receiver(SocketConnection* conn) : Protocol(conn) {
        // Build klient
        dest_ = "";
    }
    int run();
    // ~Receiver();

private:
    std::string dest_;    
};



void rsync(std::string source, std::string dest, int mod, std::string host, std::string port);

std::pair < std::vector<std::string>, std::vector<std::string> > difference (std::vector<std::string> source, std::vector<std::string> dest); //вектор cp / вектор rm
FileList GetFullDirFileList(std::string root, std::string subdir);
int BuildServer(std::string port);
int MakeClient(std::string host, std::string port);