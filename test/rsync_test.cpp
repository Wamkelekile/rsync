#include "rsync.h"

// #include <thread>

#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>

#include <gtest/gtest.h>

using namespace std;

class FakeConnection : public Connection {
public:
    Frame frame;
    virtual void WriteFrame(Frame* f) { frame = *f; }
    virtual void ReadFrame(Frame* f) { *f = frame; }
};

class FakeReceiver : public Receiver {
public:
    FakeReceiver(SocketConnection* conn) : Receiver(conn) {}
    auto get_conn() {
        return conn_;
    }
    // ~FakeReceiver();
};

// return 0 if equal
int vector_comp(std::vector<string> &res, std::vector<string> &out) {
    size_t success_times = 0;
    for (int i = 0; i < res.size(); ++i) {
        for (int j = 0; j < out.size(); ++j) {
            if (strcmp(res[i].c_str(), out[j].c_str()) == 0) {
                success_times++;
                break;
            }
        }
    }
    if (out.size() == success_times) return 0;
    else return 1;
}

TEST(Protocol, SendReceiveFileList) {
    int socks[2];
    ASSERT_EQ(0, socketpair(AF_UNIX, SOCK_STREAM, 0, socks));
    string source("/source");
    string dest("/home/wamkelekile/Dropbox/Progs/OS/Assembler");
    SocketConnection soc1(socks[0]);
    SocketConnection soc2(socks[1]);
    Sender s(&soc1, source, dest);
    FakeReceiver r(&soc2);
    FileList true_files_list = GetFullDirFileList(dest, "/");

    r.SendFileList(true_files_list);
    ASSERT_EQ(s.RecvMsg(), MsgId::FILELIST);
    FileList res = s.ReceiveFileList();
    ASSERT_EQ(0, vector_comp(res.files, true_files_list.files));

}

TEST(Protocol, SendReceiveFileStat) {
    int socks[2];
    ASSERT_EQ(0, socketpair(AF_UNIX, SOCK_STREAM, 0, socks));
    string file_name("/gmock_main.cc");
    string file_path("/home/wamkelekile/Dropbox/Progs/il-project2/contrib");
    SocketConnection soc1(socks[0]);
    SocketConnection soc2(socks[1]);
    Sender s(&soc1, "", "");
    FakeReceiver r(&soc2);
    uint32_t file_size;

    s.SendFileStat(file_path, file_name, file_size);
    ASSERT_EQ(r.RecvMsg(), MsgId::FILE);
    FileStat res = r.ReceiveFileStat();
    ASSERT_EQ(0, strcmp(res.path.c_str(), file_name.c_str()));
    ASSERT_EQ(0664, res.rools);
    ASSERT_EQ(2593, res.size);

}

TEST(Protocol, RemoveFile) {
    string file_path("/home/wamkelekile/Storage/C_build_source/fol/file");
    string path("/home/wamkelekile/Storage/C_build_source/fol/");
    FileList files = GetFullDirFileList(path, "");
    ASSERT_EQ(0, files.files.size());
    open(file_path.c_str(), O_WRONLY | O_CREAT | O_TRUNC, 0777);
    files = GetFullDirFileList(path, "");
    ASSERT_EQ(1, files.files.size());
    remove(file_path.c_str());
    files = GetFullDirFileList(path, "");
    ASSERT_EQ(0, files.files.size());

}


TEST(Protocol, SendGetList) {
    int socks[2];
    ASSERT_EQ(0, socketpair(AF_UNIX, SOCK_STREAM, 0, socks));

    string dest("/home/home/home");
    string source("/source"); 
    SocketConnection soc1(socks[0]);
    SocketConnection soc2(socks[1]);
    Sender s(&soc1, source, dest);
    FakeReceiver r(&soc2);

    s.SendGetList(dest);
    Frame f;
    r.get_conn()->ReadFrame(&f);
    ASSERT_EQ(f.msg_id, MsgId::GETLIST);
    ASSERT_EQ(0, strcmp(f.body.c_str(), dest.c_str()));
}



TEST(SenderReceiver, GetFullDirFileList) {
    string path("/home/wamkelekile/Dropbox/Progs/OS/Assembler/two");
    FileList res = GetFullDirFileList(path, "");
    FileList my_res;
    my_res.files = {"/File3.out",
    "/five/six/1.txt",
    "/five/File5.txt",
     "/three/File7.aout",
      "/three/File8.cpp"};
    ASSERT_EQ(res.files.size(), my_res.files.size());
    ASSERT_EQ(0, vector_comp(res.files, my_res.files));
}


TEST(Compare, difference) {
    std::vector<string> sor = {"a", "b", "c"};
    std::vector<string> des = {"a", "b", "d"};
    std::pair<std::vector<string>, std::vector<string>> res = difference(sor, des);
    std::vector<string> v1 = { "c" };
    std::vector<string> v2 = { "d" };
    pair <std::vector<string>, std::vector<string> > my_res = make_pair(v1, v2);
    ASSERT_EQ(0, strcmp(res.first[0].c_str(), my_res.first[0].c_str()));
    ASSERT_EQ(0, strcmp(res.second[0].c_str(), my_res.second[0].c_str()));
}




TEST(SocketConnection, ReadWrite) {
    int socks[2];
    ASSERT_EQ(0, socketpair(AF_UNIX, SOCK_STREAM, 0, socks));

    SocketConnection sender(socks[0]), receiver(socks[1]);

    Frame frame1, frame2;
    frame1.msg_id = MsgId::OK;
    frame1.body = "body";
    sender.WriteFrame(&frame1);
    receiver.ReadFrame(&frame2);
    ASSERT_EQ(frame1.msg_id, frame2.msg_id);
    ASSERT_EQ(frame1.body, frame2.body);
}
